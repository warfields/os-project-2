/**
 * This file contains implementations for methods in the Page class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "page/page.h"

using namespace std;


// Ensure PAGE_SIZE is initialized.
const size_t Page::PAGE_SIZE;


Page* Page::read_from_input(std::istream& in) {
    // If nothing return NULL PTR
    vector<char> bytes;

    for (int i = 0; i < PAGE_SIZE; i++){
        char aByte = in.get();
        if (in.eof()) break;
        bytes.push_back(aByte);
    }

    if (bytes.size() == 0) return nullptr;

    Page* neuboi = new Page(bytes);
    return neuboi;
}


size_t Page::size() const {
    return bytes.size();
}


bool Page::is_valid_offset(size_t offset) const {
    return (offset < size());
}


char Page::get_byte_at_offset(size_t offset) {
    return bytes.at(offset);
}
