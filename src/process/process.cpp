/**
 * This file contains implementations for methods in the Process class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "process/process.h"

using namespace std;


Process* Process::read_from_input(std::istream& in) {
    vector<Page*> pages;
    int numBytes;

    while (true){
        Page* page = Page::read_from_input(in);
        if (page == nullptr) break;
        pages.push_back(page);
    }

    numBytes = (pages.size() - 1) * Page::PAGE_SIZE;
    numBytes += pages.back()->size();

    Process* pntyboi = new Process (numBytes, pages);

    return pntyboi;
}


size_t Process::size() const {
    return num_bytes;
}


bool Process::is_valid_page(size_t index) const {
    return (pages.size() > index);
}


size_t Process::get_rss() const {
    return page_table.get_present_page_count();
}


double Process::get_fault_percent() const {
    if (memory_accesses == 0) return 0.0;
    return 100 * (float) page_faults / memory_accesses;
}
