/**
 * This file contains the definition of the Simulation class.
 *
 * You're free to modify this class however you see fit. Add new methods to help
 * keep your code modular.
 */

#pragma once
#include "process/process.h"
#include "virtual_address/virtual_address.h"
#include "physical_address/physical_address.h"
#include <map>
#include <list>
#include <cstdlib>
#include <iostream>
#include <cassert>
#include <boost/format.hpp>
#include <flag_parser/flag_parser.h>
#include <boost/format.hpp>
#include <process/process.h>
#include <frame/frame.h>
#include <vector>
#include <fstream>

/**
    * Class responsible for running the memory simulation.
*/

class Simulation {
    // PUBLIC CONSTANTS
    public:

    /**
        * The maximum number of frames in the simulated system (512).
    */
    static const size_t NUM_FRAMES = 1 << 9;

    // PUBLIC API METHODS
    public:

    /**
        * Runs the simulation.
    */
    void run(FlagOptions options);

    // PRIVATE METHODS
    private:

    /**
        * Performs a memory access for the given virtual address, translating it to
        * a physical address and loading the page into memory if needed. Returns the
        * byte at the given address.
    */
    char perform_memory_access(const VirtualAddress& address);

    /**
        * Handles a page fault, attempting to load the given page for the given
        * process into memory.
    */
    void handle_page_fault(Process* process, size_t page);

    /**
        * Print the summary statistics in the required format.
    */
    void print_summary();

    // Parses Flags and sets the according values
    void parse_flags(FlagOptions opts);

    // Unload a page and return the frame index that was freed
    int unload_page(Process* process, size_t page);

    // load a page
    void load_page(Process* process, size_t page, size_t frame_number);
    // INSTANCE VARIABLES
    private:
    /**
    * Whether we need to print the statistics in the csv format.
    */
    int num_processes;
    int MAX_FRAMES_PER_PROC; 
    
    bool csv;
    bool verbose;

    ReplacementStrategy strategy;
    std::map<int, Process*> processes;
    std::vector<VirtualAddress> addresses;

    std::vector<Frame*> free_frames;
    int next_open_frame = 0;
    int time = 0;
    Frame* all_frames = new Frame[NUM_FRAMES];

    int page_faults = 0;
    int total_accesses = 0;

    std::ifstream input_file;
};