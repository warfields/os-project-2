# Project 3

A simulation of a trivial OS memory manager by Samuel Warfield.

## List of files
```
├── author (Contains My Name)
├── googletest (Contains the Google C++ Unit tester)
│   ├── ...
├── inputs (Contains Test Inputs)
│   ├── process_1
│   ├── process_2
│   ├── process_3
│   └── sim_1
├── makefile (Used to Compile The Simulation)
├── Project 2 - CSCI 442 - Operating Systems.pdf (Info about the project)
├── README.md (Recurision depth exceeded)
├── src (Contains all of the src code I wrote/added to)
│   ├── flag_parser (Parses CLI Flags)
│   │   ├── flag_parser.cpp
│   │   ├── flag_parser.h
│   │   └── flag_parser_tests.cpp (Google Test Unit Tests)
│   ├── frame (Memory Frame class)
│   │   ├── frame.cpp
│   │   ├── frame.h
│   │   └── frame_tests.cpp (Google Test Unit Tests)
│   ├── main.cpp (Takes in CLI Args and starts the simulation)
│   ├── page (Page class for process data)
│   │   ├── page.cpp
│   │   ├── page.h
│   │   └── page_tests.cpp (Google Test Unit Tests)
│   ├── page_table (Organizes pages on a perprocess basis, keeping track of what is in main memory)
│   │   ├── page_table.cpp
│   │   ├── page_table.h
│   │   └── page_table_tests.cpp (Google Test Unit Tests)
│   ├── physical_address (The place some is actually in RAM)
│   │   ├── physical_address.cpp
│   │   ├── physical_address.h
│   │   └── physical_address_tests.cpp (Google Test Unit Tests)
│   ├── process (It's what the box says it is)
│   │   ├── process.cpp
│   │   ├── process.h
│   │   └── process_tests.cpp (Google Test Unit Tests)
│   ├── simulation (Main area code is implimented)
│   │   ├── simulation.cpp
│   │   └── simulation.h
│   └── virtual_address (Address that data reside at in virtual memory)
│       ├── virtual_address.cpp
│       ├── virtual_address.h
│       └── virtual_address_tests.cpp (Google Test Unit Tests)
├── verify (Folder with sample outputs for verfication of the sim)
│   ├── ...
└── verify_output_format.sh (Tests the different deliverable [IE when not final])
```

## Unusual / Interesting stuff
The Segmentation fault check occurs before the page fault detection. The order
in which these two checks occur are undocumented in the project description.
So my output for the LRU sim 2 differ on the last lines as I detect segfaults
earlier than the script that the test files were generated from. So my sim has
a small discrepency that is within the required project parameters given in the
Project 2 - CSCI 442 - Operating Systems.pdf file. Do not deduct points as this
a valid solution given the undefined edge case.

## Hours Spent To Complete
8 hours.

## Belday's Anomaly
A couple paragraphs that explain what Belday’s anomaly is and how to use your example input
file to demonstrate its effects. Be sure to:

- Define Belady’s anomaly.

Here's the definition from wikipedia:

In computer storage, Bélády's anomaly is the phenomenon in which increasing 
the number of page frames results in an increase in the number of page faults 
for certain memory access patterns. This phenomenon is commonly experienced 
when using the first-in first-out (FIFO) page replacement algorithm. In FIFO,
the page fault may or may not increase as the page frames increase, but in 
Optimal and stack-based algorithms like LRU, as the page frames increase the 
page fault decreases. László Bélády demonstrated this in 1969.

- Give the command line invocations that should be used to demonstrate 
  the anomaly with your program.

./mem-sim -f $NUM_FRAMES -s $STRAT inputs/sim_1

There are two CLI invocations that can be done to demonstrate the anomaly. Both
the replacement strategy and number of frames per process are important. As the
number frames increase (Increasing the value of $NUM_FRAMES ) in the FIFO
replacement method (The $STRAT variable), the page faults will increase. The
opposite will be true if $STRAT variable is set to LRU.

- Attempt to explain why the anomaly occurs.

In FIFO management regimes it is possible for pages to unloaded right before
they are needed causing a page fault. The loading of the needed page will
push the page that was going to be used next to be unloaded. This becomes a
cascade of sorts that cause large amounts of page faults.

